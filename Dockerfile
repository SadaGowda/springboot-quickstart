FROM openjdk:11-jdk-slim as base
WORKDIR /app
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN chmod a+x mvnw
RUN ./mvnw dependency:resolve
COPY src ./src


FROM base as build
RUN ./mvnw clean package -DskipTests

FROM base as test
CMD ["./mvnw", "test"]

FROM openjdk:11-jre-slim as install
EXPOSE 8080
COPY --from=build /app/target/*.jar /springboot-quickstart.jar

ENTRYPOINT ["java","-jar","/springboot-quickstart.jar"]