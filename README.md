##Dockerile

Hi Team, I have created the Dockerfile to build springboot-quickstart application in the current directory, it's a multistage dockerfile to build, test and install the application.
I have used to base official docker images, jdk for building and jre for running the application.
There was a fail case scenario in written test, so I have used -Dskiptests flag to proceed building the image. 

Command used to build the image locally
docker build . -t springboot-quickstart:1.1.0


##Gitlab CICD

Gitlab CICD workflow has been distributed into 3 stages, build, test and deploy.
For build and test, I have run docker command using --target flag to execute only particular stages of Dockerfile
For Deploy, I attempted to create a KinD cluster locally inside the Gitlab runner, but cluster creation and configuration some more work and time.

I have created a local cluster using Docker Desktop in my local machine, and run the below command to create the aforementioned image

kubectl run -it --rm springboot --image springboot:1.1.0 -- sh

Please review the Dockerfile and Gitlab CICD workflow file and let me know if anything more is needed from end.
Thanks.